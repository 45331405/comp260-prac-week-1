﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBetweenPoints : MonoBehaviour {

    public Vector3 startPoint;
    public Vector3 endPoint;
    public float speed = 1.0f;

    private bool movingForward = true;
    // Use this for initialization
    void Start () {
        transform.position = startPoint;
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 target;
        if (movingForward)
        {
            target = endPoint;
        }
        else
        {
            target = startPoint;
        }
        // calculate the distance to move based
        // on the speed and the frame rate
        float distanceToMove = speed * Time.deltaTime;
        // calculate how far we are from the target
        float distanceToTarget =
        (target - transform.position).magnitude;
        Debug.Log("distance to target = " + distanceToTarget);
        // check if we are close to the target
        if (distanceToMove > distanceToTarget)
        {
            // close: move straight there and change direction
            transform.position = target;
            movingForward = !movingForward;
            Debug.Break(); // pause the game
        }
        else
        {
            // otherwise just move towards the target
            Vector3 dir = (target - transform.position).normalized;
            transform.position += dir * distanceToMove;
        }

       
       
    }
}
